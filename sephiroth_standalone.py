#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  sephiroth_predict.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sys, time, re, os
from sephiroth3 import sephiroth3
verbosity = 1
#USAGE: python sephiroth_standalone input_file 
#change the following parameters in order to make it work with your local environment
HHBLITS_BIN="hhblits"
HHBLITS_DATABASE="~/hhsuite2/databases/uniprot20_2013_03/uniprot20_2013_03"
evalue = "0.01"
iterations = "3"
cpu = "4"

def main():
	if len(sys.argv) != 2 or "-h" in sys.argv[1]:
		print "\nUSAGE: python sephiroth_standalone input__file \n"
		exit(0)
	dataBase = readTrainFile(sys.argv[1])
	#dataBase = {ID:(seq, [n1, n2, n3)])}
	numProts = 0
	for target in dataBase.items(): #iterates among all the proteins with N bonds
		targetCys = target[1][1]	
		assert (len(targetCys) % 2) == 0 #just in case
		if len(targetCys) == 2:
			print "Prediction for %s is trivial: %s" % (target[0], targetCys)
			continue
		if len(targetCys) == 0:
			print "Prediction for %s is trivial: no bonded cysteines!" % target[0]
			continue
				
		print "\nPredicting protein %s" % target[0]
		open("./tmp.fa","w").write(">"+target[0]+"\n"+target[1][0])
		alignName=target[0]+"_iter"+iterations+"_eval"+evalue+"_"
		
		fileName = sys.argv[1]
		print "Building alignment with HHblits... (it may take a while) " 
		os.system("mkdir "+fileName+"AlignDir")
		os.system(HHBLITS_BIN+" -i ./tmp.fa -M first -o "+fileName+"AlignDir/"+"res -n "+iterations+" -cpu "+cpu+" -e "+evalue+" -d "+HHBLITS_DATABASE+" -oa3m "+fileName+"AlignDir/"+alignName+".a3m > /dev/null ")
		os.system("./sources/reformat.pl a3m fas "+fileName+"AlignDir/"+alignName+".a3m"+" "+fileName+"AlignDir/"+alignName+".fas")
		os.system("./sources/trim.py "+fileName+"AlignDir/"+alignName+".fas"+" "+ target[0] + " > "+fileName+"AlignDir/"+alignName+".hh")
		
		numProts += 1		

		predictedEdges = sephiroth3(fileName+"AlignDir/"+alignName+".hh", targetCys) #calculate predictions
		print "Prediction for: %s" % target[0]
		print predictedEdges
		
	os.system("rm ./tmp.fa ")	
	print "Predicted %d proteins" % numProts
	return 0
	
def readTrainFile(filename):
	dataset = {}
	totSeqs = 0
	start = time.time()

	ifp = open(filename, "r")
	if verbosity >= 1:
		print "Reading sequences from %s..." % (filename)			
	i = 0
	line = ifp.readline()
	while len(line) != 0:
		if line[0] == '>':
			#print line
			ID =  line[1:-1]

			line = ifp.readline()
			seq = ""
			while line[0:4] != "BOND":
				seq += line
				line = ifp.readline()
			if verbosity >= 4:
				print "Seq: " + seq				
			cysPos = set()
			while line != "\n":
				match = re.search(r"^BOND\s+(\d+).*", line)	
				cysPos.add(int(line[match.start(1):match.end(1)]))
				line = ifp.readline()
			if verbosity >= 4:
				print "Metadata: " + str(cysPos)
			assert len(cysPos) %2 == 0 #only even number of bonded cysteines allowed
			#cleansingXs part
			if dataset.has_key(ID):
				print "WARNING: protein: " + ID + " is already present in the database: they are not unique!"
			dataset[ID] = [seq.replace("\n",""), list(cysPos)]
			totSeqs += 1
			line = ifp.readline()
			#continue
	fine = time.time()
	
	print " Done: read %d seqs in %.3fs" % (len(dataset), (fine-start))
	return dataset # ID:(seq, [n1, n2, n3)])	
	


if __name__ == '__main__':
	main()

