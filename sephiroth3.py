#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  sephiroth3.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sources.sephirothUtils as sUtils
from sources.mwmatching import maxWeightMatching
verbosity = 0
ERROR_ALLOWED = 0
"""
MSAfile = path of the trimmed MSA produced by hhBlits
 targetCys = [int, int, ...] list containing the positions in the sequences of the cysteines we want to analyze
"""
def sephiroth3(MSAfile,  targetCys):
	targetCys.sort() #avoid any relation with the input encoding.
	assert len(targetCys) >= 2 #at leas 1 bond
	assert len(targetCys) % 2 == 0 #even number (not strictly required by the method itself)
	clusters = sUtils.clusterize(MSAfile,  targetCys)#define clusters from query sequence and MSAs
	#clusters = {name:set(seq1,seq2,...)}
	# define connections between clusters
	adjList = {} #adjacency list (at distance 2) for each cluster: all the distances needed
	edges = {}
	if verbosity >= 3:
		print "Cluster with relative adjacency list:"
	for clust in clusters.keys(): #for each cluster calculates the adjacency list (it defines a graph)
		adjList[clust] = set()
		for i in clusters.keys():
			if sUtils.positiveDoubleHammingDistance(clust, i) == 2:
				adjList[clust].add(i)
				edges[(clust,i)] = 0 #initialize all the weights between clusters to zero
		if verbosity >= 3:
			print clust,
			print " = ",
			print adjList[clust]
	#adjList = {cluster1:set(neighbor1,neighbor2,..), cluster2:set(...)}
	#edges = {((cluster1),(cluster2):counts, ((cluster1),(cluster3):counts)}
	#clusters = {UID:set(seq1,seq2,...)}
	
	#start "voting procedure" for each cluster
	for clust in clusters.items(): #each sequence votes by giving a +1 weight to a node
		#print "CLust: ", clust[0]
		for seq in clust[1]:			
			tmpAdjList = adjList[clust[0]]
			if len(tmpAdjList) > 0: #calculates to which cluster "seq" is closer and increment the weight for that edge. in this way, every sequence in clust[0] "votes"
				#print "adj: ", tmpAdjList
				#print seq
				edges[(clust[0], sUtils.calculateSingleLinkageClusteringDistance(seq, tmpAdjList, clusters))] += 1
	if verbosity >= 3:
		print "Edge weights inferred: "
		for i in edges.items():
			print i
	#transform edges in "human readable" mode: 
	hEdges = {} #human readable edges: hashtable containing all the edges (named with the position of the cys involved)
	for e in edges.items(): 
		tmp = sUtils.clusterName2Edges(e[0])
		hEdges[tmp] = hEdges.get(tmp, 0) + e[1]	#add the already calculated weigth
	if verbosity >= 2:
		print "Human Readable Edges\n", hEdges
	#prepare edges in the format recognized by Perfect Matching Algorithm
	eList = []
	for e in hEdges.items():
		eList.append((e[0][0],e[0][1],e[1]))
	if verbosity >= 3: 
		print "ELIST: ", eList
	#call the external python implementation of the Edmonds-Gabow algorithm
	matches = maxWeightMatching(eList, maxcardinality=True)[1:] 
	if verbosity >= 4: 
		print "matches: " , matches
	if len(matches) < len(targetCys):
		if verbosity >= 1:
			print "   ***   WARNING: unsufficient number of edges  ***   "		
		while len(matches) < len(targetCys):
			matches.append(-1)	
	if verbosity >= 3:
		print "Matches: ", matches
	assert len(targetCys) >= len(matches)
	edges = set()
	missing = []
	i = 0	
	while i < len(matches): #extract cys coupling
		if matches[i] == -1:
			missing.append(i)  #list of missing predictions
		else:
			edges.add(tuple(sorted([targetCys[i],targetCys[matches[i]-1]])))
		i +=1	
	if len(missing) != 0: 	#adds missing bonds by wildguessing them. some times they are "necessary" and everything's OK
		if verbosity >= 1:
			print " * Missing edges added!"
		assert len(missing) % 2 == 0
		while len(missing) > 0:
			edges.add(tuple(sorted([targetCys[missing.pop(0)],targetCys[missing.pop(0)]])))		
				
	edges = sorted(list(edges))	
	if verbosity >= 1:
		print "Edges: ", edges
	return edges

def main():
	#USAGE example:
	print sephiroth3("alignmentCys/PDBCYS_3iterE-5AlignDir/P07602_iter3_eval0.00001_.hh", [4, 7, 35, 46, 71, 77])
	return 0

if __name__ == '__main__':
	main()

