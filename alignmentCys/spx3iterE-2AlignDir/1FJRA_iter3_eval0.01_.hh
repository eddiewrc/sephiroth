DILECDYFDTVDISAAQKLQNGSYLFEGLLVPAILTGEYDFRILPDDSKQKVARHIRGCVCKLKPCVRFCCPHDHIMDNGVCYDNMSDEELAELDPFLNVTLDDGSVSRRHFKNELIVQWDLPMPCDGMFYLDNREEQDKYTLFENGTFFRHFDRVTLRKREYCLQHLTFADGNATSIRIAPHNCLIV
AELDCDYFDTVDLSGIQPDSNGSYLYENLLIPGEFTGKYDYELTGGDERIQVAEHIRGCACKLKSCVRFCCHHNLLLINSECQGNV--NEILEYDPKLNITMEDGSVAEKHVLSEFVVQRHLPLPCQETYYLDDHDEDYKWSLFENGTFLRKYDGVLFKKSQYCLQPSEL-NK---EIRLVPHNCQI-
EIPGCDYFDTVDLSQSERLPNGSYQYEKLIIPASLVGEYDYEILEKGHRESVARHLRGCACHLGTCIRFCCHRNLFLVDGECDGDT--SKAIEFDPIINITLNDGTQVRRHVLRDFIIQQDLPVPASHLYLDAENDESHQWTLFENGVLRRQFDDAELSKQAYCLQPHKIGTE---DYTLVPHNCAI-
EIPGCDFYDTVDISKAQRFSNGSYLYEGLLIPAHLTAEYDFKLLPDDSREKVASHVRGCACQLRPCIRFCCPQYHKMKKGQCYGDMSKDELDRHDPFVNVTLSDGSVVKRHFKEDIIVQSDLAKPCSRMYFLNHEMPGNGFSLFENGSLLRHWDNVVLPKREYCVQHLSFKDN---STRIAPHFCPL-
EIADCSFYDTVDISEGQRLSNGSYLYEGLLIPAHLTAKYEFKLLANGDKEQVPSHVRGCVCKLRTCVRFCCPHDHIMDMGECYANMTTEENELLDPMLNVTLDDGSVVQRHYKKELMVQWDLPKPCDDMFYLDNRDIMDEYTLFENGRLLRHYDQVYLDKSEYCLQHRTFGEGNNNSIRIIPHNCLI-
KALDCNYYDTVDLTGIQPFKNGSYPYENIIIPADLIGEYDYRLTGDDERIEVPKYIRGCACRLKSCVRFCCHHNLMFMNGECIGDT--DKALEYDPYLNITTSDGSVVEKHVLNEFVVQRHLPLPCSEIYHLDDRIEGYEWSLFENGSFKRTFDGVSLDKREYCLQPVELPDR---EIRLVPHNCQI-
DIPDCDFFDTVNITSSFKAGNGSYRYKNLTVPASLTGEYDYVIQYDGDRISVPKHIRGCVCKLKTCIRFCCPMIKRMKSFGCSKKE---NSLSYDMTLDIMQDDGSIASKHILTEMFVQEDLPLPCKTHYALDRELSSEDWTLFEDGKLLRDYDKKNLSKQEYCLQPRMASNITYYDYTIAPYNCVV-
DIPDCNFFDTVNLTNSWKFPNGSYSYRGLIIPLSLAGEYDYEIQFDGASRSVASHTRGCVCRLKPCIRFCCPLSKVMQYKRCSPQS--QNKYAYNMTLDITQDNGTVTSKHVLEDFVVQEYLPLPCGNHYALSRHNYTTDWTLFENGTLLRQYDKRYLSKQDYCLQPRRSREQIGYNYTIVPYNCSI-
DIPNCNFFDTVNISGSVPFSNGSYSHEGVFIPARLTGSYNYQIDHRGSRWTVPEHIRGCVCQLKTCVRLCCHHTQFWEAGECDKDP--EDESPWDYSVNVTLMDGSVQIMDFVKDMVNQHDTPLSCVS-YRLDARKNPDDWTLYENGALLRHYDSKWLSKMEYCMQPTASETGQVYLMA---YNCAS-
DIPNCDFFDTVKLRESEKLPDGSYRYEDVVIPAQLTGEYDYEIDYDGDRVSVPNHIRGCICKLKTCIRFCCHPKKLMAGNLCSQDV--YENLTYEYTLDITQLNGSVIRKHVLTDMVVQQDLPLPCERHYSLDAETCKYDWSLYENGSLFRHFDQRHLSKQEFCLQPNPTSTGKNYSLIVA-FNCIQ-
DIPGCNYYDTVDISYIERQ-NDSYLYDDIEIPASLTGYYEFRQFGDGSITPIEKHLRACVCSVRPCIRICCPAKNFLANGKCDDGLK-EELARFKPYIYFTY--MDLQARVPLTDMAIIRDEFFDCDEMIYI-SD-----FNYFQDGKFWV-TVDLFMEKQDYCLYRHNFDSDFPKSMWIIRHRCTS-
EIPNCKYDETINISNFKRI-NDAYIYEHFEIPANLTGEFDYKELMDGSKVPAESNLRACICKVRTCIRICCARKNILSNGECGDGVK-KELDLTMQDILLTDPPLAELNMIPQTELLVVREQFQPCDEIVSLKSD-----YTMLKDGSILLHTSAEILSNDQYCLYPEIY-ADFPKTIRIINRRCYR-
EIPPCKYDETINISHVKRF-NDAYIYEHIEIPTNLTGKFDYQELMGGSKVPVKSNLRE-ICTVRPCIRICCAYKNILSNGECNDGVK-KELDRTREDILKKAPPMAELNMVPQTKLMVLRDQFQPCDEVVSLNRD-----YKMLKDGSILLRKSATILSNDEYCLYPEIY-SDFPETIWIINRRCFK-
EIPGCDFYDTVDISKAQRFSNGTYLYEGLLIPAHLTAEYDFKLLANDSRERVATHVRGCVCQLRPCIRFCCPPYHKMHMSKCKGDMSKDEMDRHDPFVNLTLRDGSVVKRHFKEDLIVQSDLDLPCPKMYFLDHEMPGNGFTLFENGSLLRHWDTKELNKREYCVQHLSFKGK---STRIAPHFCPL-
DIAGCHFYDTVNLTSSQKFENGSYFYQGVEITPNRTGEYSHVLLPDGDRESVKSHWRGCVCQLKTCIRLCCPPNQYMTPDNCVDGL--EEETWSDHYVDITS-ENDTKSLDFHKDFVHQGHLPQPCSDLYNLYKNE----GILFENGTFLRHYDNKHLTKQEYCIQPLKFNDS----IQILPFNCAI-
VIPGCDYFDTVDISHIPKL-NDSYAYEELIIPAHLTGLYTFRQLADGSQEPVKSHLRACICKLKPCIRFCCPRNKMMPNSRCSDGLT-ENLKRINPYLKITLEDGTIGKYYLLTDMIVLRYEFRYCEKVVSVQED-----YKLYENGSFMIKPDVNWLSKQWYCLHPR---LEDPNSIWILEHVYIP-
EIADCNFLDTVNITEAERFSDGSYLYEGVLIPAHLTAKYEFKLLGNGEKEQVPSHMRGCVCKVRPCVRFCCPHDHIMNNNQCESNMTSRERKLLDPILNVTLADGSVVQRHFKKDFIVQWDLPKACDNMFYVDNREKEFQYTLFENGTFLRHLDQAYLNKREYCLQHITLNNNGVYNIRIVPQNCPV-
DIPDCSYFDTVDISNVERQ-NDSYLYDGIPIPASLTEDYDHKQFADLSTEPVKKHLRACVCKLRPCVRICCPAKYTLANGKCGDGLQ-EELARLESYIYLKSMNLSTKTRVPLTEMAIIRDPFLSCDEMMHV-TD-----FAYVENGTLYI-SDEQYFEKHYYCLYPYQFNSDFPNSMWIVKHSCVT-
DIPECNYFDTVDISNVERQ-NDSYLYDDIPIPANLTGDYDYKMFGRLRM-PDEKRLRACVCKLRPCIRICCPIKYTLANGKCVDGLQ-EELARLKSYIYLTSTDLSTKTRVPLTEMAIIRDQFLPCDEMVQL-KD-----FTYVENGTLLI-PDGQLIEKQYYCLYPYQFNSDFPNSMWIVKHSCIT-
YCNPCTEFESVDITTGVVHANGSITHDNVEYGP------------GAT---------GCPCIDHVCINKCCGRTQAFLNRSCENTD----SDTVNPFSPT----------------------RRPCNDSYGVDPSSSTEEFEVAQGGSQWH-PS------SRYCVDMSIYNNS----SS---AVCY--
EIADCSFFDTVDISLGQRLSNGSYLYEGLLIPAHLTDVYEFKLLPNGGKEQVPSHVRGCACKLRTCVRFCCPHDNLMDGGACVPNMTAEEQEILNPFLNVTLADGSVVQRHFRKDLIVQGDLPSPSCKMFSLDNRQKLEEYTLFENGTFLRHFHNVYLDKREYCLQHLTFQDNDDYSIRITPHNCLI-
DIPGCNYFDTVDISNIERQ-NDSYLYDDILIPASLTGYYEFRDFGDGSIRPIKRHLRACVCSVRPCIRICCPAKNFLPNGKCEDGLK-EELARFEPYIYLTS--MDGQKRVPLTDMAIIRDEFWDCDEMIFI-SD-----FNYFQDGTFWV-TVDRFMEKQDYCLYRHNFDSDFPKSMWIIRHRCTT-
CSLPCDFIDSVNITDGERLPNDEIRHNGVIYNK------------FAT---------GCLCAVRICVRLCCGEHE-YLGKRCTHTD----D-----YLPI----------------------GKPCEQVYELLPEESPADWSIARNGSLII-AD------NQYCLA-PRQNNG----SG----VCF--
DISGCDYFDTVDLTNSHKFENGTYLYEDILIPKEKVGLYDYQILFNGDREPVPEHTRGCACQIKSCVRFCCDPQKLLVKGECEGNI----NLNYSSILNITMHDGAEVEKDVM-EFIVQKHLPVPCNDHLMLNAAGNENHWTLFENGTLVRHFDGEHLSKRDYCLQPHRPNSQLLYELQP--HHCLP-
VIPECDYFDTVDISHIPKL-NNSYAYEGLNIPAHLTGLYAFRQLADGSQEPVKTHLRACICKLKLCIRFCCPRNKMLPNSRCSDGLT-EKLKRFNPHLKITLQDGSKKTYHLLTDMIVLRNEFRYCEKVVTVQQD-----YQLYENGSFMINPAVNWLGKQWYCLYPR---IEDPNSLWILEHVYIP-
DIPDCNYYDTVDISNVERQ-NDSYLYDDIRIPASLTGDYEYKQYRDLSREPVKKHLRACVCSLRPCIRICCPAKYILANGKCGDGLQ-EELARYESYIYLTSMNLTIDERVPLTEMTIIRDHFLACDEMMHL-TD-----YDYLEDGTLRI-SDDRYIEKQEYCLYPYQFNSDFPKSMWIVVHRCTT-
VIPGCDYFDTVDISHIPKL-NDSYAYEGVIIPGHLTGLYTFRQLADGSQEPVQTHLRACICKLKLCIRFCCARSKMLPHSRCSDGLT-ENLRRINPYLKITLQDGSRKTYHLLTDMIVLRNEFRYCERVVTVQED-----YKLFENGSFMINPAVNWLAKQWYCLYPR---LEDPNSIWLLEHVYIP-
LHVPCTFFDTVNLTGYTPFPNGSYSYEGVLIPSHLVGQYNYIYKDLVERVDVDWHPRGCICRLKACLNICCPWGQVYNKEECNNDTTDT-RIWPQPVMNVTFDNSSTQSVNIFKQFAVQ--SFRPCPKMFSLQPEINDFDYLLFQNGSMLRLDDHNYINKSEFCLVV-SYVNETDLYYSLNPANCD--
PNVPCNFYDTVNLTGHPVFPNGSYDYYGTIVPAELVGTYDYIHSSLTERIEVREHVRGCVCKFKSCLNICCPWRQVFNVDGCIIDHNDN-RTWPDPMLNITFRNESTILVNMFAQFAIQ--SFRPCPKMFSLQPEANDWDYLLFENGSMLRMDDQKLIRKNEFCMVP-TYVNESDMFYTIHPANCD--
THVACKFHDTVNLTGHTRFPNGSYEYEGIIIPEHLVGNYDYVFRSLADRVEVPNHVRGCVCKLKPCLNICCPWGQIYNRGECMNDTMDH-REWPKPVLNVTFAENITMEVNISQQFAVQ--SFRPCAKMFSLQPEINDFDYLIFQNGSMFRVDDKVFIEKNQYCLVP-TFVNESDLVYSLNPANCD--
IHTPCTFFDTVNLTGYKRFENGSYEYDGIIIPSNLVGVYDYIYKDLVQRVEVEPHMRGCICQLKSCLNLCCPWGEIY-VDRCTNDRARQ-RTWPAPVLNVTFENGTTHPVNIFQQFSVQ--SFRPCTEMFSLLPEVNEYDYVLYTNGTMLREDDMTYIRKNEFCLIP-SYVNETDTFYTLNPANCD--
IHIKCSFFDTVNLTGYPSFPNGSYNYEGVIIPSHYVGTFDYIYKDLVDRIDVTPHVRGCICKLKPCINICCPWGQIYNYTECQKDTNTK-QKWPEPLINVTLNNGSVQSVNIYEQFVVQ--SFRPCTEMFSLVPEVNSYDFQLYENGTLFREDDQHYIFKNEFCLVP-TSINDTDLYYTINPANCD--
IHTKCSFFDTVNLTGYPSYPNGSYSYEGVLIPSHYVGTFDYIYKDLVDRVDVAPHMRGCICKLKPCINICCPWGQIYNNSECQKDTNTK-QMWPEPTINITLNNGSVQNVNIYEQFVVQ--SFRPCTEMFSLLPEINYYDYQLYENGTLLREDDMHYIYKNEFCLVP-TNINDTDLYYTINPANCD--
AIPDCDYYDTVDISAAQKLPNGSYLFEGLLVPANLTAEYEFTILPDDSKQKVDKHIRGCVCKLKPCVRFCCPHNHIMDMNVCAGDMTEE-ELEVLPFLNVTLDDGSVVRRHFKKELIVQW-DLMSCDGMFSLDNREKT-DYTLFENGSFFRHHDRVTLNKREYCLQHLTFVDGNESSIRIAPHNCL--
EIPGCDFFDTVDISKAQRFSNGSYLYEGLLIPAHLTAKYDYKLLADDSKEKVASHVRGCACHLRPCIRFCCPQYQKMNMSVCYGDMSEDELNKHDPFVNVTLSDGSVVRRHFKEDLIVQSDLAKPCPRMYFMNHELPGNKFTLFENGSLLRHWDNAELSKREYCVQHLSFIGD---SIRIAPHFCPL-
TIPDCDYFDTVNVTGSQRLVNGSYLYQGVVIPAELTAEFDYRVLPDDSTEPVERHLRGCVCQLRPCLRLCCHHSQLMANGQCSGSIE-VDLTRLNPYLNVTLDDGTVARRHFQEEFIIQSDLPIPCNDMYPLNDQEEQDQYIVFENGSFFRPYDSATMSKREYCLQAYDFREGETESFRIVAHNCSI-
DIPGCNYFDTVKLTQEQQLENGSYRYRNVIIPKEKTGIYDYRISLDGKMQMASNYTRGCVCHVKSCIPFCCDPHEFL----------------VDALLNVTLFNGTETEINLRKDFLVQP-VYEECD--FIGLNSSLDHQWTLFENGILFKHTNNHNKNKGRYCLVPLAVNDD---VYKLGAHYLS--
---------------------------------------------------------------------------MLINGDCDDSL-KDQLERQDLYINVTLEDGSVSTRHFRKDLIVQWDLPMPCEYLYPVDDQNEEDQYTIYEDGSFLRHYDNKTLGKREYCLQPLKIESEDESSFRIFPHNCVI-
----------------------------------------------------------------------------MDNGVCYDNMTEEELTELDPFLNVTLDDGTVFSRHFKKELIVQWDLPMPCDGMFSLDNRDELDQYALFENGTFFRYYDRVTLNKREYCLQHLTFTDGNATSIRIAPHNCLI-
DISDCDFFDTVDLTHSFKFANGSYRYEDLIIPARLTGEYDYKILDGGTREEVSRHVRGCICKLRPCIRLCCHHKKLMSSTECSEEV--DEGLTYNYALNITLPNGNVTEKYIMNDMIVQQDLPMPCQIHYHLDAEQYAEDWTLFENSTLLRHYDNAFLYKQDYCLQPTKSKSG--EGYSVVPYNCVI-
DISDCDFLDTVDLTHSSKFENGSYRYEDLIIPARLTGEYDYRILDGGSWEHVSNHVRGCICKLRPCIRLCCHHRKLMSSTQCSKDV--DADLTYDYALDITLRNGSVTKKHIMNEMIVQQDLPMPCQIHYELDAERYAEDWTLFENGTLLRHYDNAFLSKQDYCLQPRRSKSG--ESYSLVPYNCII-
-IPGCDYFDTVDLSHSPKLSNGSYQYEGLVIPPEQTGEYDFEILADGEKESVPRHLRGCACRLGSCIRFCCHRNLFLDRNECSGDI--AKAVDYDPYVNISLANGTQVRRHVLKDFIVQQDLPVPCSEHFHLDALNDESHWTLMEDGRLLRHFDQKSLSKQEYCLQPHPISAGDNKTITLVPHNCN--
DIPECDYFDTVDLRESTRYPNGSFQYENLIIPAEQTGEYDYEILVDGEKESVKKHLRGCACKLGACVRFCCHKNLFLVQDQCSGDI--SRAVDFDPYVSITQSDGKQVIKHILDDFIVQRHLPVPCAAHHHMDAENDKNHWTLFEDGTLQRHSDKINMSKQEYCLQPHPIKTGENETITLVPHMCAV-
QQLPCEFRDSVNISGGTVDAQSNIHHDGIKYEPKHYALISYDYAGFDTRVEVPAYTRGCICQLRSCVRLCCPVGQDGNTSACVDSF--------KVRVNVS-STSGEVQVNLLEEFGVV--HLKPCAGMFPEVLD----EWSVDDFGSLQ--FMGESIPQNEYCLNV---------------------
AKTPCPLVESVDISNGVENADGSIELDGVRYGSQYFRD-------------ANSTVRGCVCLVRQCLHICCPMG-SPEGERCPP-V--------ALNVNFSMTPDGTVHVNLLDSYHLL--YFNPCSGALLTLYGD---EYIVRSDGLLA--YGASTYDYRHYCLQA---------------------
HANPCTKQESVNITEGVLYENKSIIHDGVEYVSSWYEENNNGV----------MNLFGCPCIGRLCLWKCCGKGQ---NRSCTDVV--------VHPFNPPVFKGRDPS-NLTAVFFFM--YLPSCPKYLVDSAGE---EIFLQENGTLL--EIAFKLPQWHYCLDM---------------------
DSLPCDFIDSINITDGMRDVLGNIVHNNILYKPKYYRTIDYDYEDFSTKKFVNEYIRGCPCAVRLCVRMCCPQG-TVMYDQCLPTL--------EVLVNTTLFNVQRNFVDLTENYGFL--YGKPCE----SVYE------LNRADDIWS--FA-----------RV---------------------
KQLPCEYRDSVNISSGTRDAVGNIVHNGVVYTSQNFALINYDYVDYDVRVDVANYYRGCICQVTKCVRLCCPIGQDGESTGCVDKF--------PAKILANVTTTNETKVNIIERYGFV--PQKPCPALMIEDIE----EWALDD-------------------------------------------
-------------------------YNC-IIQPSMTGEYDYEIDYDGDTVSVPNHIRGCVCKLKTCIRFCCHPKKLMVGDECSKNV--YKNLTYEYLLDITQLDGSVIKKHVLEDMVVQQDLPLPCERHYLLDAEGSKYDWSLYENGSLFRHFDQQFLSKQEFCLQPYPTSTGKN-YLLIVAFNCI--
--HPCAYADTVNITDGLRLKDGSYSYAGVVVPPELLAEYSFKVI-DGVEYRAPKHLRGCVCLLKPCISFCCPRGLVFDAKNCTAPH----QERQITHVELTYDNRTVDQVRISDRFVVR--TELGCRNKFVDKKHETFWQWDLFENGSLLR--DSRLWSTDEYCFSPLQHKAE---EWELTPLSCE--
--FPCPYADTVNITDGLRLRDGSYSFAGVLVPAELTAEYNFKVI-DGVEYKAARHLRGCVCQIKPCITFCCPPDLVFNAQNCTRPN----QVRQITHIELSASNRSVEQVKIMDHFVVR--TELGCRNKFVDSNHDSFWQWDLHANGSLQR--DDRLWSTDEYCFTPLEHN-S---AWELAPLNCE--
--FPCAYADTVNITDGLRLKDGSYSYAGVLIPPELMAEYSFKVI-DGVEYRAEKHLRGCVCQLKPCITFCCPPDLVFNAENCTKPK----QERLISHVELTYANLSVAQVKISEHFVVR--TELGCRNKFVDKKHDTFWQWDLHENGSLLR--DGRLWSTDEYCFTPLEHK-D---AWQLTPLNCE--
--HPCAYAHTVNITDGLRLKDGSYTYAGVVVPPHLMAEYSFKVI-DGVEYRAKKHLRGCVCLLKPCISFCCPENLVFDAKNCTKPH----QVRESTHVELTYANRSVEQVRIRDRFVVR--TELGCRNKFVDKKHENFWQWDLFENGTLQR--DDRLWSTDEYCFSPLEHKPE---QWELTPLNCE--
--HPCAYANTVNITDGLRLKDGSYSYAGLVIPPNMIAEYSFKVI-DGVQYRAKKHLRGCACLQKACISFCCPPDLVFDSKNCSQPH----QIRESTHIELTYGNRSVENVRIKDRFLVR--TELGCRNKFVDRKHESFWKWDLFENGSLFR--ENRVWSTDEYCFSPLEHKPE---QWELAPISCE--
--HPCPYADTVNITDGLRHKDGSYSYAGVVIPTHLMAEYNFKVI-DGVEYRAPKHLRGCVCQLKPCISFCCPNDWIFNAANCTRPD----LIRPVSHVEVTYRNRSVDQLKIQEHFVIR--TELGCRNKFVDKKHESFWQWDLYENGTLWR--DERLWNTDEYCFTPLEHKPS---VWELTPLNCE--
--YPCAFIDTANITGSFG--DGSYVHNWTVIPRNLVAAYDFVIE-NGIRVPAAKHLRGCVCKLKPCVRFCCPEGEIYDLEQCLTPQ----P--GQTHFEVQMLNGSLQELELRSRFSIY--VDTPCGHMKPVTKGSEYVHWTLHENGTIAH--RGHIFS--HYCYTPLLLHNA---SWDWQPLVCA--
--YPCAFIDTANITGSYG--DGPFVHNWTVIPRHFVAVYDFVIE-NGIRIPASRHLRACVCKTKPCVRICCLRGEIYDLEQCLVPS----S--SHSHMEVELGNGSLRLVKLQPRFSIH--VETPCEHMKAVTKGSEYVHWTLHENGTISH--RGHIFS--HYCFTPLLHGNS---TWEWQPLACA--
DIPGCDYFDTVDLSNSTQLSDGSYVFKNINIPNEKTGKYNYQIMFDGTFERIPEHTRGCICQLMSCARFCCEPQKELVKRECVGNI------------------------------------------------------------------------------------------------------
--YPCAFIDTTNISGSYSF-NGSYIHNWTVIPREWVAIYDFVIE-DGVRVPARRHLRGCVCKQTPCVRFCCPDGQIYDLKQCRQPHSMSVNPPDHSHMDIELNNGSLRHIELRSLYSIH--VETPCKHMKALRKNMEYVHWTLHENGTITH---RGHMFTKHYCFTPLQLANA---------------
--HPCAFIDTANITGGYSL-NGSYVYNWTVIPKELVTAYDFVIE-NGIRVPAARHLRACVCKLRPCVRFCCPAGQLYELRRCVAPTGDGPQPPGHSHMTVELHNGSRLSIELRARFSVH--VETPCDHMRALTKDSVYLNWTLHENGSITH---REHLFTKHYCFTPLRVGNS---------------
--HPCAFIDTVNITGSYSL-NNSYVHNWTVIPKEFVTDYDFVIE-NGIRVPTERHLRACICKQRPCVRFCCPNGQLYDLHKCVPHSAEHPQPPGHSHMTVGLRNGTQLQMEIASRFSVH--VETPCDNMRAVIKDGDYFNWTLYEDGSIIH---REHLFTKHYCYTLVLLGNR---------------
--HPCAFIDTVNITGSYSL-NNSYVHNWTVIPRELVTAYDFIIE-NGIRVPAKRHLRACICKLRPCVRFCCPAGQFYDLRSCLAAQEQLPPPPGQSHMAVQLHNGTQLDIELRSLFSVY--EETPCDHMKAYTKDNTYLNWTLHENGSIIH---RDYLFSKHYCFSPLLIGNS---------------
--YPCAFIDTANITGRVGD-NATYFYNWTPIPHHLVAVYDFVID-NGIRIPARKHLRACVCRLKPCVRICCPSGHLYDARQCSVPRSGLPPLPAHSHMEVELLNGSLRRLELGPAFSIH--IDTPCKHMKPVTKGAEYVHWTLHENGTVSH---RGHVFSKHYCFTPLLLGNS---------------
----------------------------MIIPAEQTGEYDYEILADGKKEPVKKHLRGCACKLGACVRFGCHKNLFLVQDQCRDDI--SEAVDFDPYVSITLRDGRQV--------------------------------------------------------------------------------
----------------------------------------------------------------------------MANGQCSGSIE-VDLTRLNPYLNVTLDDGTVARRHFQEEFIIQSDLPIPCNDMYPLNDQEEQDQYIVFECNENFVMANFLWLS-----------------------------
DIPGCNYFNTVDISNIERQ-NDSYLYDDILIPASLTGYYEFREFGDGSIRPIKRHLFVFESQP----RTFCPTENRMVLKRSSLGS--------NPIY--TSHP--WTERNEYHSPIWLLSEFWDCDEMIFIS----DFNYFLEQDGKFWV-TVDRFMEKQDYCLYRHNFNSDFPKSMWIIR------
--------------------------------------------------------------LWHFIRICCPRKNMLANGGCSFK---ERLFRTKLYLNITLEDNSTEALYFINQTLLTS-PFWDIDEMIGLSRDE----YTLYKNGTIYIHSDQSLKSRVEYCFYPHQIYSDFPETIWIILHK----
--------------------------------------------------------------LWHCIPICCPRKNMMANGGCSFK---EHLFRTKLDLNIRLDDNSTEALYFNNQTLLIT-SFWDIDEMMGLRRDE----YTLYKNGTIYIHSDQSIQTKEEYCFYPHQIYSDFPETIWIISHK----
--------------------------------------------------------------LWHCIRICCPRKDMMANGGCSFK---EQLFRTKLNLNIKLDDNSTETLYFNNQTLLTT-PFWDIDEMIGLTRDE----YTLYKNGTIYIHSDKRLKSKEEYCFYPHQIYSDFPETIWILLHE----
--------------------------------------------------------------LWHCIRICCPRKNMMADGGCSFK---KHLFRSKLDLSIRLDDNSTEALYFNNQTLIIT-SFWDIDEMIGLRRDE----YTLYKNGTIFLHSDQSAKTKEEYCFYPHQIYSDFPETIWIILHK----
DSLPCDFIDSVNITDGVRDSAGNIIHNNILYKPKYFRTIDYDYEHFSRKKFVAEYIRGCPCAVRLCIRMCCPYGTVFFQRKCVPTEG---QLVV--NVNTTLYNNRSSEVVDLTQ--NQMYGFKPCESVYQLDPENSPEDWIFA-RGQMG--------------------------------------
DSLPCDFIDSINITDGMRDVLGNIVHNNILYKPKYYRTIDYDYEDFSTKKFVNEYIRGCPCAVRLCVRMCCPQGTVMYDYQCLPTVNKLE-----VLVNTTLFNVQNFEVVDLTENSMYGFYGKPCESVYELNRA---DDWSFAR-------------------------------------------
---LCDTKDSVDITNGELLSNGTILSNGILYPPKHV------YFNNET---L--RTYGCVCKVKNCVRKCCSIGSVMYMN------------------------------------------------------------------------------------------------------------
--QPCHELVSVSLENAEVLAEGSVLLDGLTFPSSAQ--Y----------RDEYGQLRGCPCLVMKCIRFCCQPGEFHDPGLCYKPS------------------------------------------------------------------------------------------------------
--QPCHELVSVSLENAEVFANGSVLFDGLTFPSSAQ--Y----------RDEYGQLRGCPCLVKRCIRFCCQPGEFYDPGFCYKTR------------------------------------------------------------------------------------------------------
---------------TEVFANGSVLFDGLTFPK------------PTQYRDEKGRLRGCPCLVKKCIRFCCQPGEIHD--------------------------------------------------------------------------------------------------------------
------------VTASRLLENGSFFYDGIVYPAELRWT-------DG------NVTYGCICRLRSCIRKCCRDDEVLRNSECQKMPNNTELITEMPHLRLSRKQMTKEIQHIKNHYLVQNNVCPPGQKYYTLNPDDDSDEIILQANGSFVD-TTGKISPFWNYCIDW---------------------
---------------SRSLENGSFIYDGILYPAELRWT-------DG------NVTYGCICRSRNCIRKCCGDDEILRNKICQKMPNDTELITEIPHLRLSRNQMTKEIQHIENYYLVQDNVCPPGQQYYTLNPDDDSDEIILQANGSFVD-TAGKISPFWNYCIDWKM-------------------
----------------------------------------------------------------------------LPNSRCKDGLT-ENLKRINPYLKITLQDGTIETYYLLTDMIVLRYKFRYCEKVVTVKED----QYKLYENGSFVINPAVNWLSKQWYCLHPRL---EDQNSMWILEHVYI--
--QPCPDSLSVDISDAESLKDGTIVKNGVIFPPNAVYT--KNV-GN------TTRMFGCMCQTKDCFTKCCPLGQVIYQRNCTEMSD-KDLLKNN-GIDTFYMTDFRKHVDVKEKFNLV--YGRLCKGMFLEDSP-----WYVQEDGKLYVKIETKIYEADKYCIDT---------------------
-KLPCSFKESVNITSGESL-DNSITHNGVRYAVENYATINYSVK-NNENISVESYIRGCVCREARCVWLCCDKYSDIDGPRCDDS-------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------STELMVLREQFKPCDQVISLKRDE----YSMLKDGAMFLHKSAQTLSNDQYCLYP-EIYSDFPETIWVINRKCF--
---LCDEKNTVDLTLAKSLEDGIIISDGEKFSPG----YIYSR--NDT---GAEKIYGCLCQGRRCFRKCCPPGKVYHRRKCIDHD----DIVQTKGLNVFFMNNYKKTIDVVHTFLL---YGIPCDGDVYLEDN---GEWFIQE-------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------LQPETSSWDYLLFENGSMLRVDDKLLIRKNEFCMVP-TYVNESDMFYTIHPANCD--
-------------------------YNKLVYPAESY-----QVF--G------NKTYGCICNLQSCLRKCCKRNEILGEGNCTPLSN-E-SLIQKSLVEQNQLAAEIQQSRLSDQFFLI--EDMQCSENLLLEPENIEEDYVVQANGTLQT-FNGDKYEAWNYCLDWK--------------------
--PPCPEVLSVGIPEDERLEDGDLVHRDRTYPVHWH---------DG------NWTRGCVCHVKPCLRKCCAPGEILSTSPCQSDVGAQDELKLDPQR-------TTSSRHQLSRLSIHDFLFKPCAEKFDLNPDKYPEDHVLHENGSLYM-ESTGLLGAERFCVDRQ--------------------
---------SIDITNGTLLRNGDIVHNDFKYTKNEY-------FKRG------NSIRGCICNKKRCLLKCCPFGYGYDMKECVATSRAFEPPVYDNYA------------------------------------------------------------------------------------------
--FDCPAAERIDLTEGVPDPQGSIRFAGNVYPSENY-----------TQRSDHNHRYSCVCAVRKCVYVCCYHIN---RTECEQSA--L-------PLNRTSNRASWSDMDLLEEYWLIYATPPAWSSMYSLQIAGHEGELINVWDGSFA--YGAKVYASRTYCINPA--------------------
--QPCHDLVSVSLENVEVFANGSVLFNRLTFPNAT------------QYRDEKDQLWGCPCLIKRCIQMCCQPREFH-DKLCYKLS------------------------------------------------------------------------------------------------------
----------------VQFHNNSLIWRDQSLPSSKEAIYDFDDA-DLDRRRVPEQVRACICQKIPCIRRCCAEGEMYAKGNCKNDGSDL-KFEGFQNLNINA--------NFPTDFGIV--HGLQCPK-FRLDPDNFPDDHINPSNGSLIIHNTFKTYTNTQYCLERVRP------------------
------------------------------------------------------------CDRNPCIRKCCAENEFFYAQGCNK-RMPEEPVEYQAFVNAV--NETESTFNITKDYGVL--IGKPCHGMYPVDPKE--EKWLLTAEGHVLADYE--IFDQNKYCMDIFYNKSEFDHN-----------
------------------------------------------------------------CHRNACIRKCCAENEFFYAQGCNKLAVPDEPIEHEALANAV--NQTKSAFDTTKDYGIL--IGKPCYGMYPMDPRE--EEWALTSEGHVFVEYA--AYDQDKYCMDVFYNKSEFDHN-----------
------------------------------------------------------------CDRNVCIRKCCAEDEFFYGPECRESVGTEAPAEHPAFASAV--NATLSASGAIEDYGLL--IGKPCLRMYPVNPTE--EAWSLTSRGHVKVEYS--VQDLDKFCMDAYYNKSHYNNG-----------
------------------------------------------------------------CDTNACIRKCCDEDTYAYDD-CGRYLRSNEVGEYNALANAV--NQTNSTFDTTEDHGVL--IQLPCQNMYIYNPNE--KQVLLTSNGHVFIRYD--TND---HCFEVKHN--NFG-S-----------
------------------------------------------------------------CDTNACIRKCCPEDRYFYED-CISYLRFNEVGEYNAFVNAV--DQTNSTFDTNKSHGIL--IQKPCDLVYPYDLNE--KDVLLTSNGHVFINYD--TND---HCFEFYHS--DYN-S-----------
--------------------------------------------------------------GKACIPLCCPYGNRMTNDKCIETNN---TYRFPPLYNLTLIDETPDYR---RYFHLS--VKDPCKGWYKLSPKEYTEDFMILDNGSIYKYNKNEIFRDNDYCLGI---------------------
----CPTEALVDLTASFPDRNGSIVVDGVFYPA------------DAIRWRGNKA-LGCPCRTRGCLPLCCSHDS------CMGPVNRSTEA---EFFPVYS----PKSMQVDPDFQVSQYAWNPCHGRYILTPENPDDKFQLLSNGSMYQPTIKHLRNYTDFCVD----------------------
-----------------------------------------------------------MCNNNNCIQLCCPLGDRLVKEKCVRGEG--NYPFPDVYSY-TTSDLQSENKKVDELFLV---VHDPCETRFLLDPNEYPDDYMFLINGSLYQPRHENFLKPTSYCLAV---------------------
-----------------------------------------------------------MCENITCIPLCCPFGKRLIKEECVVGQS--NYPFPDVYEY-VINDSKPIGKKLDQIFLT---IHDPCQYRYVLEPEIRQSDYLFLSNGSLYQPNINEFIV-TSYCLAV---------------------
-----------------------------------------------------------EC-NITCIQLCCPYGYNLVKGQCVHGED--TYAFPNKQ-----GD-------LDELFLT---VRDPCI-ER--GRK-----YLFLTDGSLYMN-TGEFISPKSYCFAI---------------------
-----------------------------------------------------------AC-SITCIQFCCPFGTILIKGRCIRGED---YALSNKH-----ED-------VDELFLT---VRDPCV-GY--DRE-----YLFLTNGSLYNN-DGEHISPKSYCFAI---------------------
-----------------------------------------------------------TCQLITCIQLCCSPGDRLDDDICIPEK----IKYFLPNVY-KYTNDSLQSENKTVDELFQLTIYDPCQENRILLPYGFQYDYMFFANGSLYISYYKIFAKPTSYCLAI---------------------
-----------------------------------------------------------------CIRLCCLFGKRLVNGKCITEQG--NFIFLYEYINNSLQNESKSAD---ELFLLI--VHNPCQKTGFLNKN------VFFLNGSLYLDYYNTIIESTSYCLANVA-------------------
-----------------------------------------------------------------CIRLCCPFGSRLVKRKCIAQQGNF-IFLEKGYIN----DSSRNEKKINELFLLI--VHNPCQKTYHWF---NPYNTFFLANGSLYLSHYNTIIESTSYCLAN---------------------
---------------------------------------------------------------TGCVRKCCPNGMALNGDICQVNS---------APFEIPFHDESGAALNASSHLVVSDGVFVDCQHAYSLRPSVEGDDFYILPNGRIHTPA--LGEYHDDYCIDNF--------------------
---------------------------------------------------------------DTCIRKCCPTFKAFDISRCINYS---------SQLNVTLYNRTGYSLNLKSEMFVRDGVAPQCKDDYHLR----SNGLFVFPKKRCSSQS--QTDVDDEYCVDYF--------------------
-----------------------------------------------------------KCANITCIQLCCPLGDRLDGKNCIASEN--E----YVFPNIYSSNNSIQSKNVNDLFPLT--VQNPCQETIRFIKDSKYYEYMIFANSSLYLPNYDIFLESTSYCLAVV--------------------
-----------------------------------------------------------------------------------------------------------------------------------------------FQNGTISH---RGHIFSKHYCFTPLLHGNS---TWEWQPLACAP-
-----------------------------------------------------------FCRRNACIRKCCAENEFFYAQGCNKLAVPDEPIEFHEA----LANQTKSSFDTTKDYGIL--IGKPCKGMYPMDPRDEEWGVFLTSEGHVFIKNY-AEYDQDKYCMDIFYNKSESDHNFHL--------
-----------------------------------------------------------MCDNITCIQLCCPFGNRLVNGKCIPEQGYFVFLRNYGYINDSFQ---TESEKVDDAFLFV--VHDPCQKHYLMNPYLIQN--KFLINGSLYLPDYNTIVEPTSYCLAVVDY------------------
-------------------------------------------------------VRICFCDTNACIRKCCSEDK-YAYEGCINFLRFNEVEFYNAFVNAV--DQTNSTFDTTKDHGVL--IQQPCDQMYTYDPNEWEKQVLLTSNGHVFIHYD----ANDMHCFDVRH-------------------
---------------------------------------------------------------------CCSPGDRLDGDRCIPEK----IKYFLPNVY-KYTNDSLQSENVDELFQLT--IYDPCQENRTLLPDG--FQYMFFANGSLYISSYKIFAKSTSYCLAIT--------------------
----------------VHFHNNSLIWKDNSLPSSKFCIHDAD-L-DGGAFPEQKLVRACICQKIPCIRRCCAEGEMYAKGNCKTDGTDF-KFEGFQNLNINA-N-----FSKPSDFGIV--HGLQCPK-FRLDPDNFPDDTINASNGSLIIHNTFKTYTNTQYCVERVRPN-----------------
------------------------------------------------------AVRHCPCNQTNCIRKCCPLGMALNTTLCQTYN--E-PF-ALEFRNVT---GHVVTPNPA-SYLIRQGDAPKCQGMFPLSPNTNPEDFYVLPDGQIYLPYYPENRYTRDYCIDDFSS------------------
--------------------------------------------------------------GKPRMSLCCRVGTFLNGDNCVEATNGTEAVQLPYEMDLILANVTASD----KYFDFI--IWNPCVGMYSLNPRVYKDDWYLLSNGSIILPPEERLLDYYQYCLARVESY-EYPEYM----------
---------------------------------------------------------NEECKDITCIQFCCPYGYNLTKGQCVEHGE-DTY----AFPN-KQKNDSGESSQISDELFLT--VHDPCVARFVFA-N----TYLFLTDGSLYM-NTGEFISPKSYCFAIL--------------------
---------------------------------------------------------LCTCETGACIRKCCPENWAYVDGKCSELNASLGVFEVPRLVN---ENGTVSAYET-GLFHLVY-GKLTCGDKYLLTPSLNKTDFQITKTGWLLNESGAIIAPPDKFCLESFS--TD---NFQTLPVVCS--
--------------------------------------------------------RACD-DSITCIQFCCPFGNILTKGHCISRGE-DNYA----LPNIHG-NDSKDE--TVDELFLT--VRDPCIAGYFLFPN----TYLFLTNGSLYNNHDGEHISPKSYCFAI-MFR-----------------
--------------------------------------------------------EECT-DNITCIQLCCQNGYNLTKGRCVERGE-NKYT----LPNIYALNDSEDE--ISDELFLT--VRDPCIAGYFVFAN----TYLFLTNGSLY-TVTGEFISPKSYCFAI-LFR-----------------
--------------------------------------------------------TVCICTTTPCLRKCCLKGQVFINNTCKPRDLDKPAVFTEGYAY-------------IPDYGISRAFGDTCKKSYQLEPEVYPTDYYLITNGTLVLPNDNVAYDASQFCFENIIRNSNDDEEIILKAFVCF--
----------------------------------------------------------------PLVGKCCFLDESLVKDLCVPSND-SSTEHFSPYFSKFNQTGMLLPGKKYDQFVAI--VGIPCQKKYLLDPKADDYDYHLLLNGSIFAPQ--HLSPGENYCMEIV--------------------
----------------------------------------------------------------PLVGKCCSLGEVLAKDVCAASND-SSAMYFSPFFCDFNETGALVPGDEYKTFAAI--VGNPCDRKYMLFPEESKDDYYLLTNGSLFAPRIDHLMPGENYCMEVV--------------------
----------------------------------------------------------------PLVGKCCPVNEVLERSKCVPSTL-----YFSPLFSEFNHTGVLVPGDESSEFVAI--VGSPCDKKYMLKPEEDSKDFYLLSNGSIFAPKIDHLMPGKDYCMEVV--------------------
----------------------------------------------------------------PLVAKCCKLNQVLAKNTCTYSNS-TENEQFLPLFDEFNATGLMLPHSVQKEFVAV--VGDPCRKRYMLEPSESDEDYYLLLNGSIYAPF--HLMPGIDYCMEVA--------------------
----------------------------------------------------------------PLVGKCCSIDEVLIKDVCVSSND-SST-YFSPLFSDFNHTGALVPGDEYKTFTAL--VGNPCNQRYMLQPKENENDYYLLVNGSIFAPRLDHLMPGKNYCMEIV--------------------
----------------FKLNNGTILVNGKRFKQNEFEEENYS------TSPGKFVARYCPCGESGCIRKCCPPGMAMVTKICEPHP---------VPLNLSLLRNPKNESIDANSFSIRGGLGIKCGKL----PRDTIFDFSIRPDGQILMQLDNLPDEKDQYCIDNQV-------------------
----------------FQLDNDTLLVGEKRLESNEFEEENYS------TSPGKFVARYCPCGESGCIRKCCPPGMAMVMKICEPHP---------VPFNLSLLRNSKNESIDANSFSIRGGLGIKCGKL----PRDTIFDFSIRPDGQIVLHLENLPDEKDQYCIDNLV-------------------
-----------------------------------------------------------------RLRKCCPHGENLNIEMCDNGL-----LRFEPVISAVLYDNCIEDLEIETKLDY--DIGNPCNSSLLYDDE--EDVFFVLQDGSLLIV-DKSYTVKEHYCLDID--------------------
-----------------------------------------------------------------RLRKCCPIGENLDIEMCDNEV-----VKFEPIISAVLFDNCIEDLEIPTTLPY--DIGNPCNSSLLYEGG--EDVFFVLQDGSLLII-DKSYTVEKNYCLDID--------------------
-----------------------------------------------------------------RVRKCCPLGENLDFEMCDGAV-----LSFEPIINAVLFDNCIEDLEIFTELPY--EVGNPCNSSFQYDDT--EDLLFILQDGSLLII-DKSYTVEENYCLDQD--------------------
-----------------------------------------------------------------RLRKCCPLGENLDFEMCDNAV-----LNFEPVISAVLFDNCIEDLEIPTILPY--EIGNPCNSSFQYEDP--EDQFFVLQDGSLLII-DKSYTVEENYCLDQD--------------------
-----------------------------------------------------------ICLNKKCFRKCCPLKQSYGGRKCQRTN----HPFHPEFFRLPSLDSDN--SITLSDYGLL--LGDACEKKYLLERDE--GESYLDVSGNLYVPMYKKCFKKNDFCLEHVEFGN----------------
----------------------------------------------------------------LCIRKCCSDGEFFNSDECESSGN----------L--MASGGNPKSKEIIDKYYLLSDTFPSCENPLLLKNKETDNDFKIFKNGSIKVNKYKNLYEPHQYCGEVTWHDNG---------------
-------------------------------------------------------------------KRCCGEGEIYAKANCKRDETDI-QFQGFQNLNINA-N-----FSKPPEFGIL--HGQDCQK-FRLDPDNFPDDTISSSNGSLIIQNTRKLYTNAQYCLERVRSN-----------------
------------------------------------------------------VARGCICAKHRCLRKCCPDGQSFVNGKCRNTY--------EHGLDLAFTDRIVKELDVVEPYAIIH-KKYSCR-IYILKPRI---QFYIDTDGTFYA-FSNKTIDEKSYCIEHVQK------------------
-------------------------------------------------------VRVCFCDTNTCIRKCCPEDEYFYNGGCEKRPMPNEPEFYKAFANAV---------------------------------------------------------------------------------------
-----------------------------------------------------------ICNALACVRRCCADGEFYSKKACRRDEN---DFKFRSFES-----MSISGFTKPSEFGIL--TGLECPK-FRLDPDSFPDEVISSTNGSLFITSTEKMYSNSQYCIEKIRN------------------
---------------------------------------------------------------RVKLNKCCHLGEYLNARSCIAGNS--E--LWVPMVYLIQQQRFFEPIGASPRFKFTAHVRPVCQEQQLELFSSRLGNAFIFPNGTLSVRERLILVEPQNYCVDR---------------------
---------------------------------------------------------------RVKLNKCCHLGEYLNVQSCIAGSS--E--LWVPLVYLIQQQRFFEPTGASPRFKFIPNKRPNCSEDLVEIFSSRQANVMIFPNGTLYVRERVLLVEPSSYCVDR---------------------
---------------------------------------------------------------RVLINKCCHHGEYLIGLECVAGSS--E--KWVPLVYLKQQNRFFEPRGAKPRFKFLPNVRPPCQGQQQELFSSRDANVVLLTNGTMKVHERGLFIEPSRYCVDQ---------------------
---------------------------------------------------------------RVLLNKCCHQGEYLNGLECIAGSS--E--MWVPLVYLKQQQRFFEPKGAKPRFKFQPNVRPNCQRQQQELFSSRQANVVLFPNGTMSVRERGLLVEPASYCVDQ---------------------
---------------------------------------------------------------TVKLNKCCHSGEYLND--CIAGSE--A--LWLPMVYLVQQQRFFEPHGASPRFKFLPNTRPNCRDQTTEIFRSRGANVMLFPNGTLYVRERALMVQPSDYCVDW---------------------
-----------------------------------------------------------------MLNKCCHQGEYLNALACIAGAS--E--SWMPLVYLTQQLRFFEPKGGKPRFKLLPNVRPDCGQQQRELFSSRQANVVIFPNGTMSVRERGILVQPKNYCIDQ---------------------
---------------------------------------------------------------RVKLNKCCHMGEYLNERSCNAGSP--E--QWVPMVYLVQQQRFFEPVGGSPRFKFLPHSRPQC-DQYQELIRSRNTNVMLFPNGSLYVRERGLFVEPPNYCVDW---------------------
---------------------------------------------------------------EVRINKCCRLGEFYN-RQCVAGGIA----KWV--IFLPAKGQL--DLGSAPPF-----------RPNVYA----V-LVTLMGN-SLFLNQKHVLVP-PDY-IDE---------------------
---------------------------------------------------------------TVILNKCCPDNEFLN-SLCVSSPQ-----EWI--VFTRRLNSY--EVKELLNS-----------ENEKEEFINHY-QFLLLDN-SLW-YEGYFLNH-RSF-IDD---------------------
---------------------------------------------------------------KLTILKCCRMEELEK-SRCVPTQF-----DWK--IYSPSKQKM---LATPPEE-----------DNSVLTYVPHT-PFVIMDD-RVLIHVNDTFEP-NEY-ADS---------------------
---------------------------------------------------------------------------RLVGNNCIASKN--EY----VFPNIYSSNNSIQSKKVDELFPLT--VQNPCQEIFPMDDYSKYSKYMFFANSFLYRPFYDKFYESTSYCLAVVDH------------------
-------------------------------------------------------ARYCPCGESGCIRKCCPPGMAMVMKICEPHP---------VPFNLSLLRNPKNESIDANSFSTRGGLGIKCGKL----PRDQLLNFSMRPDGQIILHLESLPDETDQYCIENN--------------------
-----------------PAPDNAYQVNDVLWAGEPKKAYAYRPVQGG-------GYVLCTCETGACVRKCCPQNWAYVEGKCSAL-------------------------------------------------------------------------------------------------------
-----------------------------------------------------------FCDTNACIRKCCPEDKYFSEYGCIRFL------------------------------------------------------------------------------------------------------
