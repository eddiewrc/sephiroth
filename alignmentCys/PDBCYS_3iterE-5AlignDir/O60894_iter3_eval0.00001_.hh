CQEANYGALLRELCLTQFQVD-EAVGETLWCDWGRTIRSYRELADCTWH-AEKLGCFWPNAEVDRFFLAVHGRYFRSCPIS
CQDADYGALLQDLCLARFKVSMEAIGKTLWCDWAKTIGSYGELTDCTRHVADQLDCFWPNAAVHKFFVAVHQRYFKNCPAS
C-GPQYELAIDEFCLAKFRLDMQELDQGHWCSWEDTVEIYGELTNCTFLVALKVGCFWPNRLVDDFFIRVHRHYFHDCSLS
CNESLMLQN-P-ECGKYFEEMMHKVDSKKWCDIKEF--YYSKFSLCTEQKALTARCFWPNPLAEGFITEIHRQFFSKCSSV
CKSAKQLRC-P-RCSNNLDHITQPLERWKSCELTWF--YYESFTNCTEVETNVVGCYWPNPLAQSFITGVHRRYFHNCSVD
CNEKRMLAM-P-RCGKTFAEKMKKVEVWKWCNLSEF--YYESFTNCTEVETNVVGCYWPNPLAESFIASVHRQFFQNCSVD
CNRGVYKFFINDLCLANFTFNMENLDRGLWCSWKDTMEIYEGLTNCTYQVALRMNCFWPNQIVDRFFMEIHQIYFHDCALT
C-GPHYEYAIEDFCLGKFRLDMQELDQRHWCNWEDTVELYEELTNCTYLVALKMDCFWPNRLVDEFFIRIHRHYFHDCSLT
CDEGMYTYTMEDLCLKKFKFEMQDLGQRYWCDWDLTISPYRDLTNCTLLIANMLNCYWPNQLVDKFFLNVHRDYFKNCSLS
C-GSHYEFGIEDLCLAKFRLDMHELGERHWCSWEDTVELYGRLTNCTNLVAFKLGCFWPNKLVDDFFIRVHKHYFQDCSLS
CNETLMLER-P-ACGKAFEDMMKKVDTKNWCNLTEF--YYDNFTSCTELETNIVGCFWPNPLTESFITGVHKQFFSNCTSD
CNETRMLEK-P-LCGKAFADRMHKVDVWKWCNLSEF--YYESFTNCTEVETNVVGCYWPNPLAQGFISGVHRQFFANCSVD
CNEALYSHVMNEFCLQHFEIEMEELGKKLWCDWGETAGMYADLTNCTVILAENLDCFWPNHLVDHFFIVIHKKYFSNCSLT
CHEADYGQLMEDYCLSQFKFEMEVIGQKLWCDWDETADYYDELTNCTHLIANKLNCYWPNQLVDEFFIAIHKHYFKNCSLF
CNRSLYERTIHDLCLANFRLDLGGLDPGLWCSWPDTMKIYEDLTNCTHQVAHRVGCFWPNQMVDRFFMQIHRSYFHNCALT
CWDVDYDAILQEVCLAPFKVDMEAIGRTLWCDWGKTIESYEELRNCTMYVMERLNCYWPNAVVNKFFITVHQQYFRSCPVS
CQEADYGALLQKLCLTQFQVDMEAVGETLWCDWGKTIRSYRDLADCTWQVTEKLGCFWPNAEVDRFFLAVHGHYFRSCPVS
CRDPDYGTLIQELCLSRFKEDMETIGKTLWCDWGKTIGSYGELTHCTKLVANKIGCFWPNPEVDKFFIAVHHRYFSKCPVS
CQDTDHAALLRKYCLPQFQVDMEAIGKALWCDWDKTIGSYKDLSDCTRLVAQRLDCFWPNAAVDKFFLGVHQQYFRNCPVS
CQDAHYGTLMQELCLSRFQKDMEAMERTLWCDWGKTIGSYGELTDCTRNLAERLGCFWPNVEVDRFFVAVHRHYFRSCPAS
CDRGFYEKMIQKLCLANFRSNLGGLDPGLWCSWPDTMEMYEDLTNCTFQVALGVGCFWPDHVVDRFFMQIHQSYFHDCALT
CNRGFYER-INDMCLVKFKLDMGGLDRGLWCSWPDTSEIYEGLTNCTYQVASRMDCFWPNQMVDRFFMEIHRDFFRDCALT
CHEAEYGRQIHERCLRPFKLSMEGIGQQLWCDWDETMGTYGELTNCTVAVAENLTCYWPNRLVDEFFVAVHSHYFRNCSPS
CNETGMLER-P-RCGKAFADMMQKVAVWKWCNLSEF--YYESFTNCTEMETNIMGCYWPNPLAQSFITGIHRQFFSNCTVD
C-QKIYKLY-SNYCEKSFHTSMFNLSMEDWCDWNQV--YYSNLTHCIEAIAGFISCYYPDQISEELFIRIHTTFFRNCSLN
C-GSHYEFAIEEFCLAKFKLDMQMLDQRQWCSWEDTVELYSDLTNCTYIVAQGMNCYWPNRMVDEFFIQVHRYYFHNCSLS
CNRGAYEIKIDEFCQAKFRLDMMGLERSAWCSWPTTMKIYEELTNCTHQVALKMDCFWPNAVVDHFFMRVHTDYFSDCALT
CNETLMLEN-P-DCRKDFEENMKRVNPTQWCNLTEF--YYDKFSKCTESKALSYGCFWPNGLAEEFITGIHKHFFSNCTSN
C-SSHYGSAIEEFCLAKFKLDMEVLDQRQWCSWEDTVESYGELTNCTFLVALKMNCFWPNRMVDEFFIRVHRHYFHDCSLS
CNETRMLEM-P-RCGKAFADRMHEVDAWKWCNLSEF--YYDSFTNCTEEESTKVGCYWPNPLAQGLIIGIHRQFFSNCTVD
CNETGMLER-P-RCGKAFANMMRKVDVWKWCNLSEF--YYESFTNCTEAETNSVGCYWPNPLAQTFITGVHRQFFSNCTVN
CRETVYGQLIQEFCLSKFKSDMETLRTTLWCDWEKTLDWYGELTNCTFLVAAKLDCYWPNHLVDEFFVAIHKHYFKNCAVS
CPEADFGQLLNEFCLTKFKLEMDEIDKNLWCDWGQTVKSYREVTNCTYLVAQKLDCYWPGPQVNDFFITIHRNYFHDCPAT
CDQMGYADTLQELCLIHFQFQMNATSSQHWCDWDIVSSFYSELTNCSMLLAEAMSCYWPSPMVDSFFLLVHQRFFPNCSRY
CQDANYGALLQELCLTQFKVSMDAIGKTLWCDWDKTIASYGELTDCTRQVADRRGCFWPNAAVDAFFVAVHRHYFGNCPAS
CQDADHGALLQELCLSQFRVDMEAIERTLWCDWNTTIERYRELADCTRYVIEELGCYWPSTEVDKFFIAVHQHYFRSCPVS
CDETRMMEK-P-RCGKAFADVMQELDIWKWCNLSEF--YYEIFTACTEVETNTVGCYWPNHLAEGFITRIHRQFFSNCTVE
CQDTSYGPLLQELCLSQFQVSMDAIGKTLWCDWGKTIRSYSELTDCTRNVAAQLNCFWPNAAVDTFFVAIHQHYFKNCPVS
CQDADYSTLLQESCLAQFKVDMEAIGKTLWCDWGKTIGSYRELTECTSHVAKKVFCFWPNAVVDKLFVAIHQRYFRNCSVS
CNETLMLQK-P-ECGKVFEEMMKKVDAKKWCNLTEF--YYDNFTSCTELETNIVGCFWPNPLAEGFITDIHRQFFSNCTSD
CQHADHGTLLQEFCLPQFQAHMEAVGRTLWCDWGKTIRSYRELSDCTRHVVQKLDCFWPNAEVDKFFLVVHQRYFRNCPVS
CQDARYGARLKETCLIRFQQDMEAMGRTLWCDWGKTSGSYRDLTDCTRDMAERLGCFWPNVEVDKFFVAVHQRYFHSCPAS
CQEADYGWMIRHYCLKDFQDSMEGIGQRLWCDWGETMG-------------------------------------------
---------------------LPKPHVQWWLELIYFFMYYNIFTQCTEREANDASCFWPNPLAESFITGIHKQFFLNCTLD
---------------------MAGIERKFWCNWK----TYGELTNCTFFIAEKLYCTWPNLQVNDFFLAIHRHYFRECPVT
--------------------------------LRAQCKLYGRLTNCTNLVAFKLGCFWPNKLVDDFFIRVHKHYFQDCSLS
---------------------MDSIEKD-WCDW-AIIRPYSSLQNCLEQKAELFGVGFPNTWAEQVILETHQIHFANCSLV
--------------------------------------SYGELTDCTQHVAVKMGCFWPNAEVDKFFIAVHRHYFKQCPTS
---------------------WGSS----FCR------NYGELTDCTRQVAQRLRCFWPNPEVDRFFVAVHQHYFRSCSSS
--------------------------------------SYGALTHCTQQVAAQLGCFWPNAEVDKFFVAVHQHYFRSCPVS
---------------------TGGK-------MG----SYGELTNCTLLIAHKLDCYWPSQLVDTFFIAIHRFYFKHCALT
CNEDLLGKYSLLICWDQFHSEMKDINKELWCEWDQVIRPYNELTKCIESLTTYLGCYFPNQVVQDFFIQIHSSFFQACPE-
CDEQVLMFYGD-YCWAVFSDNMSTIDEQNWCVMEVVLRSYSDLTECMDAASGIAGCFYPNHVVEHLFMGIHQQYFSSCNNE
---------TE-ECWKYFVSLMGNVMTSELCEWKVISRPYSALQRCLEDWAESFNYSYPNALAEQYIFQSHHRYFHNCTLE
---------VQ-LCWQEYKVYMDSI-QKDWCDWAMISRPYSILQYCLEQTAEGFGLGFPNPWAERIIFETHQIHFANCSLV
CNESLMLEKLP-ACGKYFEEMMKKVDSKKWCNLTEFIMYYDNFTQCTEREANNASCFWPNPLAEGFITGIHKQFFSNCTSE
CNESLMLERLP-ACGKFFEEMMKKVDSKKWCNLTEFITYYNIFTQCTEREANDASCFWPNPLAESFITGIHKQFFLNCTLD
---------AE-DCWLKFYLQMTNVSKEHLCEWRAIGRPYSILRKCLEDYADKWNYGFPNALAEYYIVLSHHLYFRNCTSE
------MDAISLYCWVPFHGSMMVIEEKNWCNWNYIGRTYSDFSNCTEYLAELLGRNWPSEEMETFFVQVHSHYFENCTVE
------LDSVRDMCGSTFRKSMMLIDPRNWCNMTYILWYYHNFSNCTELVNTFLGCFWPNPLVETYIISIHKEFFLNCTSD
CRDPDYGTLIQELCLSRFKENMETIGKTLWCDWGKTIQVCR----------------------------------------
------------RCLPRFAFEMERIGASQWCNWTLVIRPYSVLSGCTELVMESLGSYWPNEEGKRLLMSQHDRYFRNCTL-
------------------------------------TRPYEELTNCTFFVALKMHCFWPNQLIDELFIHLHRDYFHDCALT
-------ILLHDECLKIFEHEMVSRNRDDWCNWGNVSGLYSNLSLCTEKVFDCLLIPWPNPFVEAIFVDIHTKFFRDCPT-
-------ALFHHFCMPKFNNAMDLLNTTDWCVWGNVNSLYSNLSICTEEISDCLQIPWPNPLVEQIFVDIHSTYFKECPT-
-------TLLQIQCITKFYDAMASLNSTEWCTWSNVGGLYSNLSLCTEEISDCLMIPWPNPLVEQTFVEIHSSHFKDCPS-
CNETGLLERLP-LCGKAFADMMGKVDVWKWCNLSEFIVYYESFTNCTEMEANIVGCYWPNPLAQGFITGIHRQFFSNCTLD
CNESRLQWEVE-VCGEDFKRNMAHIDPQHWCNLTHFISEYHLFTLCTENKSQNVNCYWPNPLVESYIIRIHKHFFSNCTLE
CNETQLLWEME-VCGEEFKRKMDHVDPQNWCNLTHFISEYHLFSLCTETNAERINCYWPNPLVESYIIRIHKHFFSNCTLE
CNESRLQWEVE-VCGEGFKEDMTHIDPQNWCNLTHFISEYHLFTLCTETKSQIVDCYWPNPLVERYIIHIHKHFFSNCTID
CNESRLQWEVE-VCGEVFKKDMDHIHPQYWCNLTYFIGEYHLFTLCTEAKSRLFHCYWPNPLVESYIIRIHKHFFSNCTNR
CNESRLRWEVE-VCGEVFKRDMGHIDPQYWCNLTHFISEYHHFTRCTELKADVVSCYWPNPLVESYIIRIHKHFFSNCSAD
CNESRLQWEVE-VCGEDFKKEMDHIKLQYWCNLTHFISEYYRFTQCTEVKSKMVHCYWPNPLVESYIIRIHKHFFSNCTNR
CNETRMLERLP-RCGKTFAERMREVAVWKWCDLSQFIVFYESFTNCTEEETVVVGCYWPNPLAQGFITGVHRQFFSNCSVD
CNESLMLEKLP-KCGKSFEEMMKRVDSKKWCNLTEFITYYNNFSICTEREANAASCFWPNSLAQGFITGIHKQFFSNCTSE
----DYETNVL-PCWYYYKTSMDSV--KDWCNWTLISRYYSNLRYCLEYEADKFGLGFPNPLAESIILEAHLIHFANCSLV
----DYETHVL-PCWYEYKSCMDSV--KDWCNWTLISRHYSDLQNCLEYNADKFGLGFPNPLAENIILEAHLIHFANCSLV
----TYDESIS-SCWEGYQDVMEDIK-EDWCDWAIISRPYSDLQECLEIEAERFGLGFPNPSAEQIIFKTHQTHFANCSLE
----NYELTAR-QCWFSYEDHMRNIST-EWCEWDMISRPYSDLQYCLEKMAEYLKLGFPNSLAEQIIFHSHQMYFANCSLE
----SYEASAQ-LCWAEYKEHMDFLE-KDWCNWTVISRPYSALRDCLEMEAEAFSLGFPNALAERFIFETHHLHFANCSLV
----SYAVTAR-QCWVFYEEYMGNISR-EWCEWDMISRPYSNLQHCLEKLAEIFKQGFPNPWAEQIIFQSHQMYFANCSLE
----NYEAAVQ-LCWNYYKYQMDPIE-KDWCDWAVISRPYSTLRDCLEHSAEEFNLGFPNPLAERIIFETHQIHFANCSLV
----NYETDAQ-LCWHDYKDYMDSIK-KDWCDWALISRPYSILQECLEQKADAFKLGFPNPWAERIIFEAHRIHFANCSLV
----NYERTIQ-FCWQSYKEEMDSIP-KDWCDWAMISRPYSDLQYCLEHFADLFQLGFPNPRAEEIIFETHQIHFANCSLV
----TYGTNVL-QCWHDYKVQMDSIK-KDWCDWALISRPYSFLRDCLEKGAEEFGLGFPNPWAEEIIFETHNIHFANCSLV
----EYHERNE-LCWQNYTSLMDSVK-GQWCNWTTISRPYSNLQSCLERNSDHFYLGFPNPVAEQFIFDTHRIHFANCSLV
----KYETYVQ-NCWLEYKKLMDPLE-KDWCDWAMIRSPYSLLRYCLEHYAEESGLGFPNPWAERAIFKTHQMHFANCSLL
----NYETAVQ-FCWNHYKDQMDSIE-KDWCDWAMISRPYSTLRDCLEHFAELFDLGFPNPLAERIIFETHQIHFANCSLV
----NYETNVL-LCWNDYKVYMDSIE-KDWCDWAVISRPYSELRDCLEQNAEEFGLGFPNPWAERIIFETHQIHFANCSLV
----DYETSVQ-FCWQSYKEQMDSIP-KDWCDWAMISRPYSDLQYCLEHFAEAFGLGFPNPWAEEVIFETHQIHFANCSLV
----SYEASAQ-LCWADYREHMDLLEK-DWCNWTVISRPYSALRDCLEVEAEVFSLGFPNPLAERVIFETHQLHFSNCSLE
---EIYTNLTH-RCWESFEQVMQNVTGTQLCEWKVISRPYSSLQQCLEYWADHLNYSYPNALAESYIFQSHHRYFQNCSA-
---EIYTNLTH-YCWESFVKVMQNVTGAQLCEWKVISRPYSSLQKCLEDHADLLNYSYPNALAESYIFQSHHHYFQNCSA-
CDRESLQYYSSNYCGQIFHQEMLTIGRDNWCVQEHFIRAYNDLTICVESVAKLTSCFFPNPNIQDFFIDIHLKYFQNCTN-
CNQKALVEGSHLYCGVPFQDEMSSLGPENWCLLPNVISPYYRMGQCLEILSTYYSCFYPNPDVQDFFLYIHSLYFHNCSR-
----CYEILVNHWCNYHFVLAMDSMNSTDWCSLEKVKSAYNNFTMCTETVADCLLVPWPNRFVEQKFVDIHATYFHDCPT-
CSDEIYKSITS-WCWEEFREWMLGVPGAQLCEWRVISRPYSNLRDCLETLADKLNYGYPNGVADKFIVLSHHTYFLNCTQ-
------------------------VNISYICDWTQTIEPYSRVTKCAEAVSLTLNCTDRRRLFDVFMLDLHRRFFANCT--
CDQHMFNSNV-EDCLSDFNTSLEMSDHQYSCPWPAVKPMYYKLKLCVDNLAITSSCMGHRSL-GDFFLKVHNMYFSSCG--
CNEDLFDINV-DNCLSDFNKSMETSDYQNMCPWPMVKSNYIKLMTCVEDWATKTWCRGYGFLVDEIYLEVHQTYFSLCG--
---TMYQEYLK-YCTSIFVENLQKLGNSDICLWPDASEPYNHLTLCLGIAANETNCKE-MMLFNRFMLAIHRHFYSHCD--
