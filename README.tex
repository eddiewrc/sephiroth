%% LyX 2.0.3 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{babel}
\begin{document}

\title{Sephiroth: unsupervised predictor of disulfide bond connectivity.}

\maketitle

\tableofcontents

\newpage

\section{Introduction}
This file aims at explaining the format and the usage of the Sephiroth unsupervised predictor of disulfide bonds connectivity in proteins.

The author of this work are D. Raimondi (\texttt{ daniele.raimondi@vub.ac.be}), G. Orlando and W.F. Vranken (\texttt{wvranken@vub.ac.be}).

The Graph WeightedMatch algorithm has been written by Joris van Rantwijk and used with his permission. This software is distributed under the GNU/GPL license.
The softwares described here can be downloaded at \texttt{http://ibsquare.be/sephiroth} .




\section{Folder Content}

Here we briefly explain the content of the folder hierarchy:\\


\texttt{sephiroth/ }

\texttt{\quad{}databases/ }

\texttt{\quad{}wrapperSrc/ }

\texttt{\quad{}sources/} ({*}) 

\texttt{\quad{}README} 

\texttt{\quad{}EXAMPLES}

\texttt{\quad{}test }

\texttt{\quad{}sephiroth.py} ({*}) 

\texttt{\quad{}sephiroth\_standalone.py }

\texttt{\quad{}sephWrapper.py} 

\texttt{\quad{}sephWrapperParall.py}~\\


\texttt{databases/} is a folder containing both PDBCYS and SPX datasets.
SPX dataset has been reformatted in order to use the same annotation
format of PDBCYS and thus be recognized by Sephiroth. \texttt{wrapperSrc/}
contains python source code common to all the \texttt{{*}Wrapper.py}
softwares. \texttt{sources/} contains auxiliary python code for \texttt{sephiroth.py}
(some utilities and the Edmonds-Gabow implementation).
\texttt{README} is this readme file. \texttt{test} is
a file containing the examples shown in the following Sections of
this manual, that can be used as input for \texttt{sephiroth\_standalone.py}
. \texttt{sephiroth.py} contains the function implementing the prediction method
described in the paper: it takes as input the path of the MSA and
the positions (starting from 0) of the bonded cysteines; it returns a
list containing the predicted connectivity. It can be imported and
called from any python script. \texttt{sephiroth\_standalone.py} contains
a standalone version that, starting from a sequence and the positions
of the bonded cysteines, builds the MSA with a local HHblits implementation
and returns the predicted connectivity. \texttt{sephWrapper.py} and
\texttt{sephWrapperParall.py} are scripts that \textit{wrap}
\texttt{sephiroth.py} and iteratively apply the prediction algorithm
to annotated dataset (such as the ones in \texttt{databases/} ).\\


It is important to notice that the algorithm explained in the related
paper is contained in \textbf{sephiroth.py} and \textbf{sources/}
(Identified with ({*}) in the folder tree). \textbf{All the other files are
auxiliary files} provided in order to facilitate the performance assessment
and the prediction of the two datasets described in the
article. 


\section{Format of the Validation Datasets}

Sephiroth algorithm predicts disulfide connectivity starting from
(1) known cysteine bonding state and (2) the query protein.


\subsection{Format of the Validation Datasets (PDBCYS and SPX)}

The following information are irrelevant from the point of view of
the "prediction algorithm" itself, but are necessary to understand \texttt{sephWrapper.py}
and \texttt{sephWrapperParall.py}, which are two
example of how \texttt{sephiroth.py} can be embedded into external scripts
and iteratively applied to sequences in order to predict their connectivity.
\texttt{sephWrapper.py} and \texttt{sephWrapperParall.py}
are provided in order to easily obtain predictions and scores for
the two datasets shown in the paper. They are equivalent but the latter
performs predictions in parallelized way, running on many CPUs at once. The
number of parallel processes desired can be set at \textbf{row 42} of \texttt{sephWrapperParall.py},in the \texttt{main} function.

In order to permit performance evaluation, PDBCYS and SPX.formatted dataset
contain sequences annotated with cysteines bonding state and connectivity,
following the sample format shown below:\\

\texttt{{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}CODE{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}}

\texttt{>3fm8|A|A0JLQ2}

\texttt{CFLVNLNADPALNELLVYYLKEHTLIGSANSQDIQLCGMGILPEHCIIDITSEGQVMLTP}

\texttt{QKNTRTFVNGSSVSSPIQLHHGDRILWGNNHFFRLNLP}

\texttt{FREE CYS 0 }

\texttt{FREE CYS 36 }

\texttt{FREE CYS 45}\\

\texttt{>2ht9|A|A0JLT0}

\texttt{LATAPVNQIQETISDNCVVIFSKTSCSYCTMAKKLFHDMNVNYKVVELDLLEYGNQFQDA}

\texttt{LYKMTGERTVPRIFVNGTFIGGATDTHRLHKEGKLLPLVHQCYL}

\texttt{FREE CYS 25 }

\texttt{FREE CYS 28 }

\texttt{SSBOND CYS 16 CYS 101}\\

\texttt{>3ce1|A|A0ZPR9}

\texttt{IKAIAVLKGDSPVQGVITFTQEXGPVTVSGEIKNMDANAQRGFHVHQFGDNSNGCTSAGP}

\texttt{HFNPTGTNHGDRTAEVRHVGDLGNVKTDASGVAKVQISDSQLSLVGPHSIIGRTIVIHAG}

\texttt{EDDLGKTDHPESLKTGNAGARSACGVIGIAA }

\texttt{SSBOND CYS 54 CYS 143 }\\

\texttt{{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}CODE{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}}

It basically is a FASTA file with cysteine-bonding
annotations in addition. 

Each query protein entry must be in the form:\\

\texttt{>pdb\_code|chain|unique\_id }

\texttt{ protein\_sequence }

\texttt{FREE CYS free\_cys\_position}

\texttt{SSBOND CYS bond\_cys\_position1 CYS bond\_cys\_position2 }\\



The line containing \texttt{>pdb\_code|chain|unique\_id}
is used to uniquely identify all the sequences in the datasets and
is read using regular expressions at \textbf{row 40} of \texttt{wrapperSrc/wrapperUtils.py},
in the \texttt{readTrainFile} function. The regular
expression can be modified in order to recognized other formats, but
an unique identifier for each sequence has to be extracted.


\subsection{Format for the Standalone Version Inputs}

The \texttt{{*}Wrapper.py} scripts are intended to evaluate Sephiroth performances
and to predict the datasets provided in the folder hierarchy. The main prediction algorithm
is contained in \textbf{sephiroth.py} and any user can call it from his preferred wrapper
script or implementation.

Sephiroth is intended to be ran on sequences with known (or predicted)
cysteine bonding state but unknown disulfide connectivity. To achieve
this task in \emph{real} case of study, the \textbf{complete annotation format} shown in the previous section is not suitable; we thus provide a particular \textit{standalone wrapper}, \texttt{sephiroth\_standalone.py}, that reads sequences in input for which the cysteine bonding states are putative or predicted (no ground truth  available). These input data should be formatted in the following manner:\\

\texttt{>unique\_id }

\texttt{ protein\_sequence}

\texttt{ BOND bond\_cys\_position1 }

\texttt{ BOND bond\_cys\_position2}

\texttt{ ... }

\texttt{ BOND bond\_cys\_positionN}\\

For example, the previously shown entries can be rewritten in the following manner. (This example of input file is available in \texttt{test} file)\\

\texttt{{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}CODE{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}}

\texttt{>A0JLQ2 }

\texttt{CFLVNLNADPALNELLVYYLKEHTLIGSANSQDIQLCGMGILPEHCIIDITSEGQVMLTP }

\texttt{QKNTRTFVNGSSVSSPIQLHHGDRILWGNNHFFRLNLP}\\

\texttt{>A0JLT0 }

\texttt{LATAPVNQIQETISDNCVVIFSKTSCSYCTMAKKLFHDMNVNYKVVELDLLEYGNQFQDA }

\texttt{LYKMTGERTVPRIFVNGTFIGGATDTHRLHKEGKLLPLVHQCYL}

\texttt{BOND 16 }

\texttt{BOND 101}\\

\texttt{>A0ZPR9 }

\texttt{IKAIAVLKGDSPVQGVITFTQEXGPVTVSGEIKNMDANAQRGFHVHQFGDNSNGCTSAGP}

\texttt{HFNPTGTNHGDRTAEVRHVGDLGNVKTDASGVAKVQISDSQLSLVGPHSIIGRTIVIHAG }

\texttt{EDDLGKTDHPESLKTGNAGARSACGVIGIAA}

\texttt{BOND 54}

\texttt{BOND 143}\\

\texttt{>A2TBB4 }

\texttt{VSKLINNGLLLVGQGAYQDLASPQQASVEQYNIIRFLGGAAPYIQNKGFGISTDIPDQCT}

\texttt{LEQVQLFSRHGERYPSTGSGKKYKAVYEKLMSYNGTFKGELAFLNDDYEYFVPDSVYLEK}

\texttt{ETSPKNSDSIYAGTTDAMKHGIAFRTKYGELFDTNDTLPVFTSNSGRVYQTSQYFARGFM}

\texttt{GDDFSNDTVKTNIISEDADMGANSLTPRDGCFNYNENANTAIVDEYTTEYLTKALNRFKA}

\texttt{SNPGLNITEDDVSNLFGYCAYELNVKGASPMCDIFTNEEFIQYSYSVDLDDYYSNSAGNN}

\texttt{MTRVIGSTLLNASLELLNHDKNENKIWLSFTHDTDIEIFHSAIGILIPDEDLPVDYTPFP}

\texttt{SPYSHVGITPQGARTIIEKYACGNESYVRYVINDAVIPIKKCSSGPGFSCNLNDYNDYVA }

\texttt{ERVAGTNYVEQCGNNNASAVTFYWDYETTNYTASLINS}

\texttt{BOND 210 }

\texttt{BOND 431 }

\texttt{BOND 258 }

\texttt{BOND 271 }

\texttt{BOND 401 }

\texttt{BOND 409}\\

\texttt{{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}CODE{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}}\\

In this case, the first sequence \textquotedbl{}A0JLQ2\textquotedbl{}
doesn't contain cysteines annotated to be bond, so it is ignored by
the program. Also the connectivity of the other two entries is trivial
and thus no computation is performed. The protein called \textquotedbl{}A2TBB4\textquotedbl{}
contains 6 proteins annotated to be bond and so it is a valid example
of query sequence to be predicted.


\section{HHblits Aligment Method}

\texttt{sephiroth\_standalone.py} can use a local
version of HHblits (Remmert et al., 2012) for building MSA of the
query sequences. The path of HHblits binaries have to be specified
in \textbf{rows 29-33} of \texttt{sephiroth\_standalone.py}.

HHblits can be downloaded from here:\\ \texttt{ftp://toolkit.genzentrum.lmu.de/pub/HH-suite/}\\
and a detailed pdf manual can be found here:\\ \texttt{ftp://toolkit.genzentrum.lmu.de/pub/HH-suite/hhsuite-userguide.pdf} .


\section{Command Line Usage Instructions}

All the scripts show usage hints if called with wrong number
of arguments or if the \texttt{-h} string is used as first argument. In this section we describe more in detail how to use these scripts from command line.


\subsection{sephWrapper.py and sephWrapperParall.py}

These scripts are intended to run predictions on pre-computed alignments
(provided in \texttt{alignmentCys/} folder) for PDBCYS and SPX datasets. They are only
wrappers for the Sephiroth algorithm that is implemented as single
function in \texttt{sephiroth.py} .

The command line is the following:\\

\texttt{\$ python sephWrapper.py dataset\_path numBonds MSA\_dir\_path
MSA\_suffixes}\\

\texttt{database\_path} is the path of the dataset (PDBCYS or SPX).
These datasets can be found in the \texttt{databases/} folder.

\texttt{numBonds} is an integer number representing which class of
proteins you want to predict (proteins with 2, 3, ..., N bonds).

\texttt{MSA\_dir\_path} is the path of the folder containing the pre-computed
MSAs. Precomputed MSAs can be found in \texttt{alignCys/} folder,
for example we suggest to use \texttt{alignmentCys/SSsequenze3iterE-2AlignDir}.

MSA\_suffixes are the suffixes of the files containing the MSAs in
the selected folder. All suffixes have to be the same within each folder, because
all the MSAs in each folder have been calculated with the same parameters. An example
of suffix, coherent with the folder used in the previous example is
\texttt{\_iter3\_eval0.01\_.hh} .

For example, the proteins with 2 bonded cysteines in the SPX set can
be predicted by typing the following command:\\

\texttt{\$ python sephWrapperParall.py databases/SPX.formatted 2\\ alignmentCys/spx3iterE-2AlignDir
\_iter3\_eval0.01\_.hh}\\

the same can be done, using the script without parallelism, for PDBCYS
with the following command:\\

\texttt{\$ python sephWrapper.py databases/PDBCYS 2 \\alignmentCys/PDBCYS\_3iterE-2AlignDir
\_iter3\_eval0.01\_.hh}\\

For obtaining the predictions for PDBCYS dataset (with and without parallelism), the \texttt{bash} command lines are the following:\\

\texttt{\$ python sephWrapper.py databases/PDBCYS 9 \\alignmentCys/PDBCYS\_3iterE-2AlignDir/ \_iter3\_eval0.01\_.hh }\\

\texttt{\$ python sephWrapperParall.py databases/PDBCYS 9 \\alignmentCys/PDBCYS\_3iterE-2AlignDir/ \_iter3\_eval0.01\_.hh }\\

\subsection{sephirot\_standalone.py}

The \texttt{sephirot\_standalone.py} script embeds the prediction function and is able to predict
sequences \textit{with} known cysteine bonding states but \textit{without}
known disulfide connectivity (and without precomputed alignments). It can be called with the following
command line:\\

\texttt{\$ python sephirot\_standalone.py input\_file}\\

where \texttt{input\_file} is a file containing protein sequences, IDs and
annotations of the oxidized cysteines as explained in the previous sections of this
README.

The script is able to retrieve the entries and,  \textbf{iff} the correct paths
are set at \textbf{rows 29-33} of \texttt{sephirot\_standalone.py}, calculate HHblits'
Multiple Sequence Alignments (MSAs) for all the sequences. For each
sequence, the corresponding predicted connectivity is printed on the
screen as a list of tuples.

An example of the output of the execution of \texttt{sephirot\_standalone.py}
script on the example shown in Section 3.2 (available in \texttt{test} file) is the following:\\

\texttt{{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}CODE{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*} }

\texttt{draimondi@misterbig:\textasciitilde{}/sephiroth> python sephiroth\_pipeline.py
test }

\texttt{Reading sequences from test... Done: read 3 seqs in 0.001s }

\texttt{Prediction for A0ZPR9 is trivial: {[}54, 143{]} }

\texttt{Prediction for A0JLQ2 is trivial: {[}16, 101{]}}

\texttt{Predicting protein A2TBB4 }

\texttt{Building alignment with HHblits... (it may take a while) }

\texttt{mkdir: cannot create directory `testAlignDir': File exists }

\texttt{inserting gaps... }

\texttt{Reformatted testAlignDir/A2TBB4\_iter3\_eval0.01\_.a3m with
1437 sequences from a3m to fas and written to file testAlignDir/A2TBB4\_iter3\_eval0.01\_.fas }

\texttt{Prediction for: A2TBB4 {[}(210, 431), (258, 271), (401, 409){]} }

\texttt{Predicted 1 proteins}

\texttt{{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}CODE{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*}{*} }


\section{Output}

The final prediction is in the following form: 

\texttt{{[}(210, 431), (258, 271), (401, 409){]}}

It means that cysteine at position 210 is predicted to form a disulfide
bond with cys at position 413, cys at position 258 is predicted to
be bond to cys at position 271 and also cysteines at positions 401
and 409 form a disulfide bond.
\end{document}
