#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  sephWrapperParall.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from wrapperSrc.wrapperUtils import *
from sephiroth3 import sephiroth3
import sys
from multiprocessing import Pool
verbosity = 0

#PREDICTION PERFORMANCES EVALUATION PARAMETERS
stat = [0,0,0,0]
predictedConnectivity = {}

def main():
	if len(sys.argv) != 5 or "-h" in sys.argv[1]:
		print "\nUSAGE: python sephWrapperParall.py database_path numBonds MSA_dir_path MSA_suffixes num_cores[opt]\n"
		exit(0)
	dataBase = readTrainFile(sys.argv[1])
	numBonds = int(sys.argv[2])
	TARGET_MSA_DIR=sys.argv[3]
	TARGET_MSA_NAME=sys.argv[4]
	NUM_PROCESSORS=4
	if len(sys.argv) == 6:
		NUM_PROCESSORS=int(sys.argv[5])
	predictions = {}
	numProts = 0
	
	# MAIN ITERATION with thread launcher
	pool = Pool(processes=NUM_PROCESSORS)
	for target in dataBase.items(): #iterates among all the proteins with N bonds
		targetCys = getBonded(target[1][2])		
		assert (len(targetCys) % 2) == 0 #just in case
		if len(targetCys) == numBonds*2:
			numProts += 1
			assert not predictions.has_key(target[0])			
			pool.apply_async(execution, args = (target, targetCys, TARGET_MSA_DIR, TARGET_MSA_NAME), callback=logResults)
			
	pool.close()
	pool.join()
	Qprot = stat[0]
	print numProts
	print stat[1]
	assert numProts == stat[1]
	Rb = stat[2]
	edgeTot = stat[3]
	print "*******************SCORES**********************"
	print "Num of proteins with %d bonds: %d" % (numBonds, numProts)
	print "Qp = %3.3f" % (Qprot/float(numProts))
	print "Rb = %3.3f" % (Rb/float(edgeTot))	
	
	return 0
	
def execution(target, targetCys, TARGET_MSA_DIR,TARGET_MSA_NAME):
	if verbosity >= 1:
		print "\nPredicting protein %s" % target[0]
	if  TARGET_MSA_DIR[-1] != "/":
		TARGET_MSA_DIR += "/"
	predictedEdges = sephiroth3(TARGET_MSA_DIR+target[0]+TARGET_MSA_NAME, targetCys)
	groundTruth = extractCouplingFromMeta(target[1][2])
	########## assessing results
	tot = 1
	Qprot = 0
	Rb = 0
	edgeTot = 0			
	if verbosity >= 1:
		print "Prediction: ", predictedEdges
		print "real      : ", groundTruth
	if predictedEdges == groundTruth:
		print "CORRECT\n"
		Qprot = 1
	else:
		print "WRONG!!!\n"
	for i in predictedEdges:
		if i in groundTruth:
			Rb += 1
	edgeTot += len(groundTruth)	
	return (target[0], predictedEdges, Qprot, tot, Rb, edgeTot)


def logResults(results):
	if verbosity >= 1:
		print "Logging"
	stat[0] += results[2] #Qprot
	stat[1] += results[3] #tot num prots
	stat[2] += results[4] #rb
	stat[3] += results[5] #tot numEdges
	predictedConnectivity[results[0]] = results[1] #store connectivity predicted
	




if __name__ == '__main__':
	main()

